CREATE TABLE Usuario
(
    [Id] INT NOT NULL IDENTITY(1,1) PRIMARY KEY,
    [usuario] VARCHAR(50) NOT NULL,
    [contrasena] VARCHAR(250) NOT NULL,
    [intentos] INT NOT NULL,
    [nivelSeg] DECIMAL NOT NULL,
    [fechaReg] DATE NOT NULL DEFAULT GETDATE()
)